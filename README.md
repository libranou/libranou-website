
# Libranou

<!-- badges: start -->
<!-- badges: end -->

This is the source code of libranou.re website.


## TODO list


- ajouter le nom de domaine prestalibre.re à gitlab pages (dans paramètres du projet) --> OK

- ajouter tracking avec Plausible --> OK

- améliorer les cartes de présentation en ajoutant les info : services et langages préférés --> OK

- open-souce : parler de transparence --> OK

- compléter les textes manquant dans les missions --> à valider par **tous**

- illustrations des missions --> **tous**

- logo et favicon --> **aliko** ?

- css : couleurs en fonction du logo

- gérer le css de la partie "nos valeurs"

- nuage de tags pour l'illustration (mots clés dans brainstrom)

- Infographie pour expliquer ce qu'est le libre

- Charte "CHATON-like" (cf liens dans le chat)

